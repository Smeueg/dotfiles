#!/bin/sh
# ------------------------------------------------
# Script Name    - droidcam-start
# Author Name    - Smeueg
# Author Email   - Smeueg@gmail.com
# Author Gitlab  - https://gitlab.com/Smeueg
# Last Updated   - Wed  2 Mar 12:00:14 WIB 2022
# ------------------------------------------------

# Variables
arguments="$@"
modulesToLoad='' # modules that will be loaded
modulesError=''  # modules that needs to be loaded but doesn't exist
video=true
audio=false
kill=false
unload_module=false

while [ ${#@} -gt 0 ]; do
	case "${1}" in
		"kill"|"-k") kill=true ;;
		"unload-module"|"-u") unload_module=true ;;
	esac
	shift
done


Kill() {
	if printf '%s' "${arguments}" | grep kill >/dev/null; then
		pkill droidcam-cli
		exit
	fi
}


ModuleExists() {
	moduleName="$1"
	! modinfo $moduleName 2>&1 | grep "ERROR: Module $moduleName not found" >/dev/null
	return $?
}


ModuleLoaded() {
	moduleName="$1"
	lsmod | grep "$moduleName" >/dev/null
	return $?
}


LoadModules() {
	if [ ! -z "${modulesError}" ]; then
		printf 'The following modules are required, however does not exist in this system:\n'
		printf '%s' "${modulesError}" | xargs -I {} printf '    {}\n'
		exit 1
	fi


	if [ ! -z "${modulesToLoad}" ]; then
		printf 'The following modules are going to be loaded:\n'

		for module in $(printf ${modulesToLoad}); do
			printf "  * ${module}\n"
			cmd="modprobe ${module};${cmd}"
		done

		cmd="su -c '${cmd}'"
		if ! eval "${cmd}"; then
			printf "Wrong password"
			exit
		fi
	fi


}


UnloadPulseaudioModule() {
	while read line; do
		if [ ! "${line##*droidcam_audio*}" ]; then
			pactl unload-module ${line%%[!0-9]*}
		fi
	done <<-EOF
	$(pactl list modules short)
	EOF
}


LoadPulseaudioModule() {
	printf '\n-------------\n\n'
	printf 'Loading pulseaudio module...\n'
	if pactl list | grep droidcam_audio >/dev/null; then
		printf 'Pulseaudio module is already loaded, skipping...'
	else
		pactl load-module module-alsa-source device=hw:Loopback,1,0 source_properties=device.description=droidcam_audio >/dev/null &&
			printf ' Loaded\n' ||
			printf ' Failed\n'
	fi

	pactl list | grep droidcam_audio >/dev/null &&
		printf 'Setting default source to alsa_input.hw_Loopback_1_0... ' &&
		pactl set-default-source 'alsa_input.hw_Loopback_1_0' >/dev/null &&
		printf 'Success\n' || printf 'Failed\n'
}


HandleArg() {
	arg="$1"
	module="$2"

	if printf '%s' "${arguments}" | grep "${arg}" >/dev/null; then
		if ! ModuleLoaded ${module}; then
			ModuleExists ${module} &&
			modulesToLoad="${module}\n${modulesToLoad}" ||
			modulesError="${module}\n${modulesError}"
		fi

		return 0
	fi

	return 1
}


main() {
	if [ "$(pidof droidcam-cli)" ]; then
		pkill droidcam-cli
	fi

	if [ -z "${arguments}" ]; then
		droidcam-cli
		tput bold
		tput setaf 3
		printf 'Please add arguments\n'
		tput sgr 0
		exit
	fi

	${kill} && Kill
	${unload_module} && UnloadPulseaudioModule && exit

	if HandleArg '\-a' 'snd_aloop';    then audio=true; video=false; fi
	if HandleArg '\-v' 'v4l2loopback'; then             video=true; fi

	if ${audio} && ! pidof pulseaudio >/dev/null; then
		printf 'Pulseaudio is not running, exiting...\n'
		exit
	fi

	LoadModules
	droidcam-cli ${arguments} &
	${audio} && sleep 3 && LoadPulseaudioModule

	printf '\033[1mRun `droidcam-start kill` or `killall droidcam-cli` to stop droidcam\033[0m\n'
}
main
